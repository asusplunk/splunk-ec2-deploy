# Ansible scripts to build a Splunk cluster in EC2

## What you need in AWS
* VPC
* Two security groups
    * Public group: allow all traffic in and out
    * Private group: allow all traffic in from 10.0.0.0/16 and all traffic out
* 5 subnets
    * Public subnet: 10.0.0.0/16
    * Private subnet: 10.0.1.0/24
    * SH1 subnet in availability zone A: 10.0.2.0/24
    * SH2 subnet in availability zone B: 10.0.3.0/24
    * SH3 subnet in availability zone C: 10.0.4.0/24
    * Default route for all subnets should be an Internet Gateway
    * All subnets should be configured to auto-assign public IPs
* Elastic IP for bastion host
* Elastic Load Balancer
    * Target Group for web port: HTTP port 8000, stickiness enabled set to 1 day
    * Target Group for management port: HTTPS port 8089
    * HTTP load balancer with forwarding rules for respective ports, spanning all three search head subnets
    * Health checks modified to allow any success or redirect status (200-399)
    * After launching instances, add search head instances to target groups
* Your own Splunk license, in `assets/splunk.license`
* Grep for "changeme" in this file and set your own Splunk admin password and pass4SymmKey

## Launching cluster with Ansible
Run the playbooks in this order.
### Create EC2 instances
* `./provision_bastion.yaml` - Create the bastion host.
* `./provision_cluster.yaml` - Create the cluster nodes.

### Set up the bastion host
* `./setup_ssh.yaml` - Create an SSH key for the centos user and copy it to all the cluster nodes, and create users on the bastion configured in the variables of this playbook.
* `./install_bastion_software.yaml` - Update all software on the bastion and install x2go, MATE and XFCE, pip, and the AWS command line tools.

### Install Splunk on the nodes
* `./disable_thp.yaml` - Disable Transparent Huge Pages on any node that will run Splunk
* `./install_splunk.yaml` - Install Splunk Enterprise on all the nodes and a Universal Forwarder on the bastion.
* `./license_splunk.yaml` - Install a Splunk license on the SH Deployer node, configure it as a license master, and configure the other nodes as license slaves to it.

### Configure Splunk indexer clustering
* `./launch_idx_cluster.yaml` - Configure the cluster master and its nodes, including indexer discovery for forwarders.

### Configure Splunk search head clustering
* `./launch_sh_cluster.yaml` - Perform the initial configuration of the SH deployer and point the search heads at it.
* `./elect_sh_captain.yaml` - Elect search1 as the captain, bootstrapping the SH cluster for the first time. ***ONLY RUN THIS ONCE***, as specified in the warning at the beginning of the script. Running it on an already-functioning cluster can bork things.

### Benchmark the cluster
* `./splunk_benchmark.yaml` - Upload and input a large data set for benchmarking.

## Caveats/things left to be done
* HTTPS for SplunkWeb should be left disabled until we can generate a cert for the elastic load balancer.
* Monitoring console on the idx cluster master has all necessary data, but must be configured manually through SplunkWeb.
* Should eventually get human-readable domain names for the load balancer and bastion.
* Users are only created on the bastion. Need to think of a solution to authentication, since I can't exactly ask users to provide _private_ keys for SSHing from the bastion to cluster nodes.
* Splunk users aren't created. Only admin is available.
