#!/usr/bin/python
from splunklib import client
import json

service = client.connect(host='localhost', port=8089, username='admin',
                         password='changeme')

jobs = service.jobs
kwargs_blockingsearch = {
    'exec_mode': 'blocking',
    'earliest_time': '0',
    'latest_time': 'now'
}

dense_search = 'search sourcetype=access_combined | stats count by clientip'
sparse_search = 'search sourcetype=access_combined status=301 | stats count by clientip'
rare_search = 'search sourcetype=access_combined useragent="*Nexus One*" | stats count by clientip'

dense_job_run = jobs.create(dense_search, **kwargs_blockingsearch)
dense_job = {
    'bench_type': 'dense',
    'sid': dense_job_run['sid'],
    'eventCount': dense_job_run['eventCount'],
    'resultCount': dense_job_run['resultCount'],
    'runDuration': dense_job_run['runDuration']
}
print(json.dumps(dense_job))
dense_job_run.cancel()

sparse_job_run = jobs.create(sparse_search, **kwargs_blockingsearch)
sparse_job = {
    'bench_type': 'sparse',
    'sid': sparse_job_run['sid'],
    'eventCount': sparse_job_run['eventCount'],
    'resultCount': sparse_job_run['resultCount'],
    'runDuration': sparse_job_run['runDuration']
}
print(json.dumps(sparse_job))
sparse_job_run.cancel()

rare_job_run = jobs.create(rare_search, **kwargs_blockingsearch)
rare_job = {
    'bench_type': 'rare',
    'sid': rare_job_run['sid'],
    'eventCount': rare_job_run['eventCount'],
    'resultCount': rare_job_run['resultCount'],
    'runDuration': rare_job_run['runDuration']
}
print(json.dumps(rare_job))
rare_job_run.cancel()
